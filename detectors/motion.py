from lib.config import Configuration
from lib.detector import Detector


class MotionDetector(Detector):
    def __init__(self, config='motion.json', camera=None):
        super().__init__()

        if not camera:
            raise Exception("A 'camera' is required for this type of detector.")

        self.config = Configuration(config)

        self.camera = camera
        stream, self.prev_buffer = self.camera.capture_sample()

        self.threshold = self.config.get('threshold', 30)
        self.sensitivity = self.config.get('sensitivity', 30)

    def detect(self):
        changed_pixels = 0
        stream, buffer = self.camera.capture_sample()
        motion_detected = False
        max_pixdiff = 0

        if buffer:
            for x in range(0, self.camera.scratch_width):
                for y in range(0, self.camera.scratch_height):
                    pixdiff = abs(self.prev_buffer[x, y][1] - buffer[x, y][1])

                    if pixdiff > max_pixdiff:
                        max_pixdiff = pixdiff

                    if pixdiff > self.threshold:
                        changed_pixels += 1

                    if changed_pixels > self.sensitivity:
                        motion_detected = True
                        break
                if motion_detected:
                    break

        if motion_detected:
            self.add_to_queue()

        # Swap buffer for next comparison.
        self.prev_buffer = buffer

        return motion_detected

    def add_to_queue(self):
        if self.get_queue_length() > self.queue_max:
            # Discard oldest entry - this is very bad!
            self.pop_from_queue()

        (stream, buffer) = self.camera.capture()
        self.queue.insert(0, stream)
        print("Motion - capture buffer length: (%i)" % self.get_queue_length())

    def close(self):
        print("Closing motion module.")
        self.camera.close()
