#!/bin/bash
set -e
set -x

if [ "$1" = 'monitor' ]; then
    python3 main.py
fi

exec "$@"
