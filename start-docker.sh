#!/bin/bash

TAG_NAME="jkennedy901/monitorclient:latest"

if [ "$(uname -m)" == "armv6l" ]; then
    TAG_NAME="jkennedy901/monitorclient-rpi1:latest"
fi

echo "Running image from $TAG_NAME"

docker run -it --privileged \
        -v $PWD:/home/monitorclient \
        -v /opt/vc:/opt/vc \
        --name monitorclient \
        --restart=always \
        $TAG_NAME
