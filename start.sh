#!/bin/bash

ENV_DIR=$(eval echo ~${SUDO_USER})"/Projects/monitorclient/venv/bin"
APP_DIR=$(eval echo ~${SUDO_USER})"/Projects/monitorclient"

start_motion()
{
    echo 'Starting monitor client...'
    # it's necessary to cd to the APP_DIR
    # main.py requires relative files like motion.conf
    cd $APP_DIR
    python main.py > /dev/null 2> /dev/null &
}

echo 'Activating required environment...'

if [ -d "$ENV_DIR" ]; then
    echo 'Environment exists, activate it.'
    source $ENV_DIR/activate

    start_motion
else
    echo 'Error: client virtualenv missing!'
    echo $ENV_DIR
    exit 1
fi

echo 'Complete!'
