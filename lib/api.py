from datetime import datetime
import os

import coreapi
from coreapi.utils import File


class MonitorClient(object):
    def __init__(self, endpoint=None):
        if not endpoint:
            raise Exception("MonitorClient requires 'endpoint' parameter upon construction.")
        self.endpoint = endpoint
        self.schema = None
        self.client = coreapi.Client()

    def login(self, username='', password=''):
        self.client = coreapi.Client(
            auth=coreapi.auth.BasicAuthentication(
                username=username,
                password=password
            )
        )

        self.load_schema()

        return self

    def get_schema(self):
        return self.client.get("{0}/schema/".format(self.endpoint))

    def load_schema(self):
        self.schema = self.get_schema()
        return self

    """
    /accounts
    """

    def list_accounts(self):
        return self.client.action(self.schema, ['accounts', 'list'])

    def read_account(self, id=None):
        try:
            return self.client.action(self.schema, ['accounts', 'read'], params={'id': id})
        except coreapi.exceptions.ErrorMessage as e:
            return None

    """
    monitorapi/devices
    """

    def list_devices(self, offset=0, limit=10):
        return self.client.action(self.schema, ['monitorapi', 'devices', 'list'], params={'offset': offset, 'limit': limit})

    def create_device(self, name="untitled device", account=""):
        # mc.client.action(mc.schema, ['monitorapi', 'devices', 'create'], params={'account':'http://45.76.120.39/accounts/4/'})
        return self.client.action(self.schema, ['monitorapi', 'devices', 'create'], params={'name': name, 'account': account})

    def read_device(self, id=None):
        try:
            return self.client.action(self.schema, ['monitorapi', 'devices', 'read'], params={'id': id})
        except coreapi.exceptions.ErrorMessage as e:
            return None

    def touch_last_contact(self, id=None):
        lc = datetime.now().isoformat()
        return self.client.action(self.schema, ['monitorapi', 'devices', 'update'],
                                  params={'id': id, 'last_contact': lc})

    def delete_device(self, id=None):
        return self.client.action(self.schema, ['monitorapi', 'devices', 'delete'], params={'id': id})

    """
    monitorapi/incidents
    """

    def create_incident(self, file=None, filename=None, device_id=None):
        """
        :param file:
        :param filename:
        :param device_id:
        :return: 
        """
        # If a file is provided, attempt to acquire the name.
        # We can set the name manually, or this will provide a default name anyway.
        if file and not filename:
            if hasattr(file, 'name'):
                filename = os.path.basename(file.name)
            else:
                filename = datetime.now().strftime('%Y-%m-%d_%H%M-%S-%f')

        if not device_id:
            raise Exception("'device_id' is always required upon incident creation.")

        params = {'device': '/monitorapi/devices/{0}/'.format(device_id)}

        # A file is provided, upload it with the payload.
        if file:
            params['file'] = File(filename, file)

        try:
            response = self.client.action(
                self.schema
                , ['monitorapi', 'incidents', 'create']
                , params=params
                , encoding='multipart/form-data'
            )
        except Exception as e:
            print(e)
            raise(e)

        # Release the file (if any) when we're done.
        if file and hasattr(file, 'close'):
            file.close()

        return response
