import os
import threading
import time


class Uploader(threading.Thread):
    def __init__(self, lock, pool):
        super(Uploader, self).__init__()
        self.lock = lock
        self.pool = pool

        self.stream = None
        self.device_id = None
        self.client = None

        self.hash = hex(abs(hash(os.urandom(8))))

        self.event = threading.Event()
        self.terminated = False
        self.start()

    def run(self):
        while not self.terminated:
            if self.event.wait(1):
                try:
                    print("{0}: upload processor is running...".format(self.hash))
                    if self.stream:
                        retries = 0
                        success = False
                        max_retries = 3
                        while retries < max_retries:
                            try:
                                print("{0}: creating incident: {1}".format(
                                    self.hash, time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime()))
                                )

                                self.client.create_incident(file=self.stream, device_id=self.device_id)

                                print("{0}: incident created: {1}".format(
                                    self.hash, time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime()))
                                )

                                retries = max_retries
                                success = True
                            except Exception as e:
                                retries += 1
                                print("{0}: incident upload failed, will retry ({1})...".format(self.hash, retries))
                                time.sleep(3)

                        # If not successful or always store images locally?
                        # Local store comes into play at this point?
                        if not success:
                            print("{0}: incident upload failed completely.".format(self.hash))
                finally:
                    self.event.clear()
                    with self.lock:
                        print("{0}: upload processor returned to pool.".format(self.hash))
                        self.pool.append(self)
