import os


class Util(object):
    @staticmethod
    def dot_read(obj, prop, default=None):
        if prop in obj:
            return obj[prop]
        return default

    @staticmethod
    def get_used_space(path):
        if not path:
            raise Exception("Can't 'get_used_space' without 'path' parameter.")
        return sum(os.path.getsize(os.path.join(path, f)) for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))) / 1024 / 1024

    @staticmethod
    def purge_oldest_files(path=None, fallback_space_to_reserve=8, filename_prefix='', extension='.jpg'):
        if not path:
            raise Exception("Can't 'purge_oldest_files' without 'path' parameter.")
        # If path doesn't exist, make it so
        if not os.path.exists(path):
            os.makedirs(path)

        # Start deleting files until we're no longer over the limit
        if Util.get_used_space(path) > fallback_space_to_reserve:
            for filename in sorted(os.listdir(path)):
                if filename.startswith(filename_prefix) and filename.endswith(extension):
                    os.remove(os.path.join(path, filename))
                    if Util.get_used_space(path) < fallback_space_to_reserve:
                        return

    def __init__(self):
        pass
