

class Detector(object):
    def __init__(self):
        self.queue = []
        self.queue_max = 80
        self.prev_buffer = None

    def detect(self):
        """
        Perform detection and return if anything was detected at this point in time.
        :return: 
        """
        raise Exception("'detect' not implemented for this detector.")

    def get_queue_length(self):
        return len(self.queue)

    def add_to_queue(self):
        """
        Grab a full sample and add an entry to the internal queue.
        :return: 
        """
        raise Exception("'add_to_queue' not implemented for this detector.")

    def pop_from_queue(self):
        """
        Take an entry from the internal queue.
        :return: 
        """
        return self.queue.pop(0)

    def close(self):
        """
        Shutdown and close resources.
        :return: 
        """
        raise Exception("'close' not implemented for this detector.")
