from models.incident import Incident
from models.base import db


class LocalStore(object):
    """
    Provides a generic way to interact with persistent data.
    """
    def __init__(self):
        self.db = db
        self.db.connect()
        self.tables = [
            Incident
        ]

    def bootstrap(self):
        self.db.create_tables(self.tables)

    # def purge_db(self):
    #     self.db.drop_tables(self.tables)
    #
    # def bootstrap_db(self):
    #     self.db.create_tables(self.tables)
    #
    # def reset_db(self):
    #     self.purge_db()
    #     self.bootstrap_db()

    def create_incident(self, path=None, filename=None):
        Incident.create(
            path=path,
            filename=filename
        )
