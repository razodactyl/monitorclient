from peewee import *
from models.base import BaseModel
from datetime import datetime


class Incident(BaseModel):
    path = CharField()
    filename = CharField()
    datetime = DateTimeField(default=datetime.now)
    uploaded = BooleanField(default=False)
