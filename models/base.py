from peewee import *

db = SqliteDatabase('monitor.db')


class BaseModel(Model):
    class Meta:
        database = db
