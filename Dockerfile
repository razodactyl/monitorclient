FROM balenalib/raspberrypi3-python:3.7.4-latest
# Sun 1st Mar 2020 - Pinned to 3.7.4 - recent downloads of 3.7 would install 3.7.6 which has issues with picamera lib.
# See: https://github.com/waveform80/picamera/issues/604

ENV LANG C.UTF-8
# https://askubuntu.com/questions/909277/avoiding-user-interaction-with-tzdata-when-installing-certbot-in-a-docker-contai
ENV DEBIAN_FRONTEND=noninteractive

RUN mkdir -p /home/monitorclient
WORKDIR /home/monitorclient
COPY ./requirements.txt /home/monitorclient

RUN apt-get update && apt-get install -y libtiff5-dev libjpeg-dev zlib1g-dev \
    libfreetype6-dev liblcms2-dev libwebp-dev libharfbuzz-dev libfribidi-dev \
    tcl8.6-dev tk8.6-dev python-tk build-essential
RUN pip install Pillow

ENV READTHEDOCS=True
# Don't check that we're running on Raspberry Pi (Cross platform build.)

RUN pip install -r requirements.txt

# # Need userland Raspbian libraries for 'libbcm_host.so' etc.
# RUN apt-get update &&  apt-get install -y libraspberrypi0 libraspberrypi-dev libraspberrypi-doc libraspberrypi-bin

ADD entrypoint.sh /entrypoint.sh
RUN chmod 777 /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["monitor"]
