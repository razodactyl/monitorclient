#!/bin/bash

DOCKERFILE="Dockerfile"

if [ "$(uname -m)" == "armv6l" ]; then
    DOCKERFILE="Dockerfile.rpi1"
fi

echo $DOCKERFILE

sudo docker build -t monitorclient -f $DOCKERFILE . \
	&& sudo docker run -it --privileged \
	-v $PWD:/home/monitorclient \
        -v /opt/vc:/opt/vc \
	--restart=always \
        monitorclient
