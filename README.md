# Monitor Client

Physical monitoring application, submits incident reports according to specified sensors.

Originally designed for the Raspberry Pi camera system.

----

Create 'motion.json' file with contents like `motion.example.json`.

Install additional libraries (if running without Docker):

```
sudo apt-get update && sudo apt-get install python-picamera python3-picamera libopenjp2-7 libtiff5
```

Install requirements (if running without Docker):

```
python3 -m venv /home/pi/Projects/monitorclient/venv
source /home/pi/Projects/monitorclient/venv/bin/activate
pip3 install -r /home/pi/Projects/monitorclient/requirements.txt
```

Setup networking:

See https://wiki.archlinux.org/index.php/WPA_supplicant

```
wpa_cli
scan
scan_results
add_network
set_network N ssid "SSID"
set_network N psk "PSK"
save_config
enable_network N
```

Development / Enable Samba:

- https://hub.docker.com/r/trnape/rpi-samba
- `docker run -it --rm -p $(hostname -I | awk '{ print $1 }'):445:445 -v /home/admin:/share/data trnape/rpi-samba -u "jonathan:" -s "Home:/share/data:rw:jonathan"`
- Finder > CMD+K > smb://192.168.x.x
- `/home/pirate` should be mounted, assuming the Hypriot raspberry pi image which has this as the default user.

---

Additional resources:

- https://www.docker.com/blog/multi-arch-images/
- http://collabnix.com/building-arm-based-docker-images-on-docker-desktop-made-possible-using-buildx/
