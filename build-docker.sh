#!/bin/bash

DOCKERFILE="Dockerfile"
TAG_NAME="jkennedy901/monitorclient:latest"

if [ "$(uname -m)" == "armv6l" ]; then
    DOCKERFILE="Dockerfile.rpi1"
    TAG_NAME="jkennedy901/monitorclient-rpi1:latest"
fi

docker buildx build --platform linux/arm64,linux/arm/v7 -t $TAG_NAME -f $DOCKERFILE --push .
