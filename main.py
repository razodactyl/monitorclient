import os
import threading
import time

from cameras.pi import PiCamera
from detectors.motion import MotionDetector
from lib.api import MonitorClient
from lib.uploader import Uploader
from lib.config import Configuration
from lib.util import Util

"""
1. Motion detection loop.
2. Motion detected, start recording for 30 seconds.
3. Recording stopped, pass to incident processor
3a. Incident is processed, leave local copy if settings permit
3b. Incident is processed, local copy only exists in volatile storage
4. Loop for other maintenance tasks such as checking for updates or logging statistics
5. how to attempt to keep device online?
6. Should be offline first, so incidents are at least logged locally.
7. how do we orchestrate the token swap? why doesn't it work with coreapi?
8. configuration cli app? login with username / password to acquire token?
9. HTTPS?
10. Local logging?
11. Testing?
"""

"""
from lib.api import MonitorClient

mc = MonitorClient(endpoint="http://45.76.120.39")
mc.login(username='test', password='user')
print(mc.list_devices(0, 10)['results'])
"""

if __name__ == "__main__":
    config = Configuration('motion.json')

    should_ping = config.get('should_ping')
    ping_timeout = config.get('ping_timeout')
    upload_processor_count = config.get('upload_processor_count', 10)

    endpoint = config.get('endpoint')
    username = config.get('username')
    password = config.get('password')
    account_id = config.get('account_id')
    device_id = None
    device_name = config.get('device_name')

    print("Connecting to: `{0}` as `{1}`...".format(endpoint, username))

    mc_api = MonitorClient(endpoint=endpoint)
    logged_in = False

    try:
        mc_api.login(username=username, password=password)
        logged_in = True
    except Exception as e:
        print("Login failed:", e.error)

    if logged_in:
        """
        Account & Device Orchestration:
        1. If a device named `device_name` is found under this user, no further configuration is required.
        2. If the device doesn't yet exist, an account containing the user is required to submit a new device.
            - No account, can't continue.
            - Account found, store `account_url` for device creation.
                - Create device named `device_name` belonging to `account_url`.
        3. Acquire device id as `device_id`
        """

        def find_device_by_name(name=""):
            print("Searching for registered device called: `{0}`".format(device_name))

            devices = []
            offset = 0
            limit = 10
            paginate = True

            while paginate:
                result = mc_api.list_devices(offset, limit)
                if result.get('next'):
                    offset += limit
                else:
                    paginate = False
                devices += result['results']

            for d in devices:
                if d.get('name') == name:
                    print("Found first device called: `{0}` at `{1}`".format(name, d.get('url')))
                    return d
            return None

        device = find_device_by_name(device_name)

        if not device:
            print("Device not found, creating: `{0}`".format(device_name))
            print("Requesting account info for:", account_id)
            account_url = None
            account = mc_api.read_account(account_id)
            if account:
                account_url = account.get('url')
            if account_url is None:
                print("No matching account found for this user...")
            else:
                print("Found account: `{0}`".format(account.get('name')))
                device = mc_api.create_device(device_name, account_url)
                print("Device created:", device)

        if device:
            device_id = device.get('id')

    if logged_in and device_id:
        fallback_dir = os.path.join(config.get('fallback_dir', '/home/pi/default_fallback_dir'), str(device_id))
        fallback_space_to_reserve = config.get('fallback_space_to_reserve', 16)

        pool_lock = threading.Lock()
        pool = []

        # Instantiate motion detector as 'm'
        m = None

        print("Acquiring upload processors.")

        for i in range(upload_processor_count):
            pool.append(Uploader(pool_lock, pool))
        try:
            print("Acquiring detectors.")

            # camera = NoiseCamera()
            camera = PiCamera(options=config.get('camera_options', {}))
            m = MotionDetector(camera=camera)
            running = True
            time_now = time.time()

            print("Starting incident detection loop.")

            while running:
                if m.detect() or m.get_queue_length() > 0:
                    print("Motion logged, attempting to upload incident.")
                    with pool_lock:
                        print("Searching for available uploader...")
                        if pool:
                            print("Found available processor.")
                            processor = pool.pop()
                        else:
                            print("No processor available right now...")
                            processor = None
                    if processor:
                        print("Running upload processor...")

                        processor.client = mc_api
                        processor.stream = m.pop_from_queue()
                        processor.device_id = device_id
                        processor.archive_dir = fallback_dir

                        processor.event.set()
                    else:
                        print("Upload processor pool exhausted. Waiting for more uploaders...")

                if should_ping:
                    if time.time() - time_now > ping_timeout:
                        """
                        Timeout:
                        Ping server so it knows this device is still online.
                        - space left on the device
                        - current cpu usage
                        - current ram usage
                        """
                        # Reset the timer.
                        time_now = time.time()
                        print("mc_api.touch_last_contact(id={0})".format(device_id))
                        mc_api.touch_last_contact(device_id)

                Util.purge_oldest_files(path=fallback_dir, fallback_space_to_reserve=fallback_space_to_reserve)
        except Exception as e:
            print("Encountered exception during main processing loop")
            print(e)
        finally:
            print("Cleaning up...")
            if m:
                m.close()
            while pool:
                processor = pool.pop()
                processor.terminated = True
                processor.join()
                print("Processor terminated...")
            print("Program ended.")
