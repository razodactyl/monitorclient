from cameras.base import Camera
from PIL import Image


class NoiseCamera(Camera):
    def __init__(self):
        """
        NoiseCamera:
        - Returns fake noise images to trick a motion detector that enough pixels have changed.
        - Useful for testing.
        """
        super().__init__()
        self.i = 1

    def get_next_index(self):
        self.i = self.i + 1
        if self.i > 3:
            self.i = 1
        return self.i

    def capture(self):
        try:
            f = open("./data/testing/noise{0}.jpg".format(self.get_next_index()), 'rb')
            im = Image.open(f)
            buffer = im.load()
            im.close()
            stream = f
            stream.seek(0)
            return stream, buffer
        except Exception as e:
            return None

    def capture_sample(self):
        return self.capture()
