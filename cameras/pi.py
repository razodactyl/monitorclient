from cameras.base import Camera
import picamera
import io
from PIL import Image
from datetime import datetime

from lib.util import Util


class PiCamera(Camera):
    def __init__(self, resolution=(640, 480), scratch=(100, 75), quality=80, options={}):
        super().__init__()

        self.quality = quality

        self.scratch_width, self.scratch_height = scratch

        self.camera = picamera.PiCamera()
        self.camera.resolution = resolution
        self.set_from_options(options)

    def set_from_options(self, options):
        self.camera.hflip = Util.dot_read(options, 'hflip', False)
        self.camera.vflip = Util.dot_read(options, 'vflip', False)
        self.options = {
            'use_video_port': Util.dot_read(options, 'use_video_port', False),
            'video_quality': Util.dot_read(options, 'video_quality', 25),
            'video_record_seconds': Util.dot_read(options, 'video_record_seconds', 5)
        }

    def capture_image(self, resize=None, quality=None, use_video_port=None):
        stream = io.BytesIO()

        # If quality isn't specified, falls back to internal setting.
        if quality is None:
            quality = self.quality

        # If not explicitly defined, revert to default.
        if use_video_port is None:
            use_video_port = Util.dot_read(self.options, 'use_video_port')

        self.camera.capture(stream, format='jpeg', resize=resize, quality=quality, use_video_port=use_video_port)

        stream.seek(0)

        try:
            im = Image.open(stream)
            buffer = im.load()
            im.close()
            stream.seek(0)
        except Exception as e:
            return None
        return stream, buffer

    def capture(self, resize=None, quality=None, use_video_port=None):
        stream = io.BytesIO()

        if quality is None:
            quality = self.quality
        if use_video_port is None:
            use_video_port = Util.dot_read(self.options, 'use_video_port')

        self.camera.resolution = (1296, 730)
        # Quality between 1-40 with 1 being highest.
        self.camera.start_recording(stream, format='h264', quality=Util.dot_read(self.options, 'video_quality'))
        self.camera.wait_recording(Util.dot_read(self.options, 'video_record_seconds'))
        self.camera.stop_recording()

        stream.seek(0)

        # The local API client will provide a default filename if we don't provide it.
        # It doesn't guess the extension however - so we'll provide the name manually here.
        stream.name = datetime.now().strftime('%Y-%m-%d_%H%M-%S-%f.h264')

        return stream, None

    def capture_sample(self):
        return self.capture_image(resize=(self.scratch_width, self.scratch_height), quality=100, use_video_port=False)

    def close(self):
        self.camera.close()
