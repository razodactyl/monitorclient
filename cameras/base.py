
class Camera(object):
    def __init__(self):
        self.scratch_width = 100
        self.scratch_height = 75

    def open(self):
        pass

    def capture(self):
        pass

    def capture_sample(self):
        pass

    def close(self):
        pass
