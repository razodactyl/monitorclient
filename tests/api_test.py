import os
import unittest


class TestApiClient(unittest.TestCase):
    def setUp(self):
        from lib.api import MonitorClient
        from lib.config import Configuration

        self.config = Configuration('../test_config.json')

        username = self.config.get('username')
        password = self.config.get('password')
        endpoint = self.config.get('endpoint')

        self.mc = MonitorClient(endpoint=endpoint)
        self.mc.login(username=username, password=password)

    #
    # Devices
    #

    def test_device_creation(self):
        # Create device and assert the creation name matches.
        response = self.mc.create_device("a-test-device")
        self.assertEqual('a-test-device', response['name'])

    def test_can_delete_device(self):
        # Create device then make sure it can be read back.
        response = self.mc.create_device("a-test-device")
        id = response["id"]
        response = self.mc.read_device(id=id)
        self.assertEqual(response['id'], id)

        # Delete device then make sure it's not returned.
        self.mc.delete_device(id=id)
        response = self.mc.read_device(id=id)
        self.assertIsNone(response)

    def test_can_acquire_existing_device(self):
        pass

    #
    # Incidents
    #

    def test_incident_creation(self):
        response = self.mc.create_device("a-test-device")
        id = response["id"]

        with open('../data/testing/noise1.jpg', 'rb') as f:
            response = self.mc.create_incident(file=f, device_id=id)
            print(response)

        self.mc.delete_device(id=id)


if __name__ == '__main__':
    unittest.main()
